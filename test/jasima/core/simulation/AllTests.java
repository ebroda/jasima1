package jasima.core.simulation;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * 
 * @author Torsten Hildebrandt
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({ TestSimulationBasics.class })
public class AllTests {

}
